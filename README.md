# Services Directory

# About Blue Rose
Blue Rose is a server infrastructure geared towards hands-off adminitration, rock-solid stability.

We are currently geared towards the creative industry, particularly the storage-heavy needs of animation companies and virtual production infrastructures. Blue Rose is particularly well positioned for these industries because of it's stability and modularity.